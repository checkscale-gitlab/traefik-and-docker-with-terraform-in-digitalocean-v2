# Create a new domain
resource "digitalocean_domain" "domain" {
  name = "yourdomain.com"
}

# Add a record hg for the jboss in the domain
resource "digitalocean_record" "www" {
  domain = "${digitalocean_domain.yourdomain.name}"
  type   = "A"
  name   = "master"
  ttl    = "35"
  value  = "${digitalocean_droplet.web.ipv4_address}"
}

# Add a record load balancer in the domain
resource "digitalocean_record" "lb" {
  domain = "${digitalocean_domain.yourdomain.name}"
  type   = "A"
  name   = "lb"
  ttl    = "36"
  value  = "${digitalocean_droplet.web.ipv4_address}"
}
